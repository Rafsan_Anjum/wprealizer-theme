<?php
/**
 * WP Realizer Single Class
 *
 * @since 1.0.0
 *
 * @package wprealizer
 */

if ( ! defined( 'ABSPATH' ) ) {
    return;
}

if ( ! class_exists( 'WPrealizerSingle' ) ) {
    class WPrealizerSingle {

        /**
         * WPrealizerPage constructor
         *
         * @since 1.0.0
         */
        public function __construct() {
            add_action( 'wprealizer_single_post_top', [ $this, 'single_post_top' ] );
            add_action( 'wprealizer_single_post_content', [ $this, 'single_post_content' ] );
            add_action( 'wprealizer_single_post_bottom', [ $this, 'single_post_bottom' ] );
            add_action( 'wprealizer_post_header_before', [ $this, 'post_header_before' ] );
            add_action( 'wprealizer_post_content_before', [ $this, 'post_content_before' ] );
        }

        /**
         * Page top content setups
         *
         * @since 1.0.0
         *
         * @return void
         */
        public function single_post_top() {
            ?>
            <header class="entry-header">
                <?php

                /**
                 * Functions hooked in to wprealizer_post_header_before action.
                 *
                 * @hooked wprealizer_post_meta - 10
                 */
                do_action( 'wprealizer_post_header_before' );

                if ( is_single() ) {
                    the_title( '<h1 class="entry-title">', '</h1>' );
                } else {
                    the_title( sprintf( '<h2 class="alpha entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
                }

                do_action( 'wprealizer_post_header_after' );
                ?>
            </header><!-- .entry-header -->
            <?php
        }

        /**
         * Page top content setups
         *
         * @since 1.0.0
         *
         * @return void
         */
        public function single_post_content() {
            ?>
            <div class="entry-content">
                <?php

                /**
                 * Functions hooked in to wprealizer_post_content_before action.
                 *
                 * @hooked wprealizer_post_thumbnail - 10
                 */
                do_action( 'wprealizer_post_content_before' );

                the_content(
                    sprintf(
                    /* translators: %s: post title */
                        __( 'Continue reading %s', 'wprealizer' ),
                        '<span class="screen-reader-text">' . get_the_title() . '</span>'
                    )
                );

                do_action( 'wprealizer_post_content_after' );

                wp_link_pages(
                    array(
                        'before' => '<div class="page-links">' . __( 'Pages:', 'wprealizer' ),
                        'after'  => '</div>',
                    )
                );
                ?>
            </div><!-- .entry-content -->
            <?php
        }

        /**
         * Page bottom content setups
         *
         * @since 1.0.0
         *
         * @return void
         */
        public function post_header_before() {
            if ( 'post' !== get_post_type() ) {
                return;
            }

            // Posted on.
            $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';

            if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
                $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
            }

            $time_string = sprintf(
                $time_string,
                esc_attr( get_the_date( 'c' ) ),
                esc_html( get_the_date() ),
                esc_attr( get_the_modified_date( 'c' ) ),
                esc_html( get_the_modified_date() )
            );

            $output_time_string = sprintf( '<a href="%1$s" rel="bookmark">%2$s</a>', esc_url( get_permalink() ), $time_string );

            $posted_on = '
			<span class="posted-on">' .
                /* translators: %s: post date */
                sprintf( __( 'Posted on %s', 'wprealizer' ), $output_time_string ) .
                '</span>';

            // Author.
            $author = sprintf(
                '<span class="post-author">%1$s <a href="%2$s" class="url fn" rel="author">%3$s</a></span>',
                __( 'by', 'wprealizer' ),
                esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
                esc_html( get_the_author() )
            );

            // Comments.
            $comments = '';

            if ( ! post_password_required() && ( comments_open() || 0 !== intval( get_comments_number() ) ) ) {
                $comments_number = get_comments_number_text( __( 'Leave a comment', 'wprealizer' ), __( '1 Comment', 'wprealizer' ), __( '% Comments', 'wprealizer' ) );

                $comments = sprintf(
                    '<span class="post-comments">&mdash; <a href="%1$s">%2$s</a></span>',
                    esc_url( get_comments_link() ),
                    $comments_number
                );
            }

            echo wp_kses(
                sprintf( '%1$s %2$s %3$s', $posted_on, $author, $comments ),
                array(
                    'span' => array(
                        'class' => array(),
                    ),
                    'a' => array(
                        'href'  => array(),
                        'title' => array(),
                        'rel'   => array(),
                    ),
                    'time' => array(
                        'datetime' => array(),
                        'class'    => array(),
                    ),
                )
            );
        }

        /**
         * Page bottom content setups
         *
         * @since 1.0.0
         *
         * @return void
         */
        public function post_content_before() {
            wprealizer_post_thumbnail();
        }

        /**
         * Page bottom content setups
         *
         * @since 1.0.0
         *
         * @return void
         */
        public function single_post_bottom() {
            ?>
            <div class="entry-footer">
                <?php
                // use for future
                ?>
            </div><!-- .entry-footer -->
            <?php
        }
    }
}

new WPrealizerSingle();

