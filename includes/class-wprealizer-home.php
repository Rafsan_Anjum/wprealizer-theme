<?php
/**
 * WP Realizer Home Class
 *
 * @since 1.0.0
 *
 * @package wprealizer
 */

if ( ! defined( 'ABSPATH' ) ) {
    return;
}

if ( ! class_exists( 'WPrealizerHome' ) ) {
    class WPrealizerHome {

        /**
         * WPrealizerPage constructor
         *
         * @since 1.0.0
         */
        public function __construct() {
            add_action( 'wprealizer_home_top', [ $this, 'home_top' ] );
            add_action( 'wprealizer_home_content', [ $this, 'home_content' ] );
            add_action( 'wprealizer_home_bottom', [ $this, 'home_bottom' ] );
        }

        /**
         * Page top content setups
         *
         * @since 1.0.0
         *
         * @return void
         */
        public function home_top() {
            if ( is_front_page() && is_page_template( 'template-fullwidth.php' ) ) {
                return;
            }

            ?>
            <header class="entry-header">
                <?php
                wprealizer_post_thumbnail( 'full' );
                the_title( '<h1 class="entry-title">', '</h1>' );
                ?>
            </header><!-- .entry-header -->
            <?php
        }

        /**
         * Page top content setups
         *
         * @since 1.0.0
         *
         * @return void
         */
        public function home_content() {
            ?>
            <div class="entry-content">
                <?php the_content(); ?>
                <?php
                wp_link_pages(
                    array(
                        'before' => '<div class="page-links">' . __( 'Pages:', 'wprealizer' ),
                        'after'  => '</div>',
                    )
                );
                ?>
            </div><!-- .entry-content -->
            <?php
        }

        /**
         * Page bottom content setups
         *
         * @since 1.0.0
         *
         * @return void
         */
        public function home_bottom() {
            ?>
            <div class="entry-footer">
                <?php
                // use for future
                ?>
            </div><!-- .entry-footer -->
            <?php
        }
    }
}

new WPrealizerHome();

