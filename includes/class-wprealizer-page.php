<?php
/**
 * WP Realizer Page Class
 *
 * @since 1.0.0
 *
 * @package wprealizer
 */

if ( ! defined( 'ABSPATH' ) ) {
    return;
}

if ( ! class_exists( 'WPrealizerPage' ) ) {
    class WPrealizerPage {

        /**
         * WPrealizerPage constructor
         *
         * @since 1.0.0
         */
        public function __construct() {
            add_action( 'wprealizer_page_top', [ $this, 'page_top' ] );
            add_action( 'wprealizer_page_content', [ $this, 'page_content' ] );
            add_action( 'wprealizer_page_bottom', [ $this, 'page_bottom' ] );
        }

        /**
         * Page top content setups
         *
         * @since 1.0.0
         *
         * @return void
         */
        public function page_top() {
            if ( is_front_page() && is_page_template( 'template-fullwidth.php' ) ) {
                return;
            }

            ?>
            <header class="entry-header">
                <?php
                wprealizer_post_thumbnail( 'full' );
                the_title( '<h1 class="entry-title">', '</h1>' );
                ?>
            </header><!-- .entry-header -->
            <?php
        }

        /**
         * Page top content setups
         *
         * @since 1.0.0
         *
         * @return void
         */
        public function page_content() {
            ?>
            <div class="entry-content">
                <?php the_content(); ?>
                <?php
                wp_link_pages(
                    array(
                        'before' => '<div class="page-links">' . __( 'Pages:', 'wprealizer' ),
                        'after'  => '</div>',
                    )
                );
                ?>
            </div><!-- .entry-content -->
            <?php
        }

        /**
         * Page bottom content setups
         *
         * @since 1.0.0
         *
         * @return void
         */
        public function page_bottom() {
            ?>
            <div class="entry-footer">
                <?php
                // use for future
                ?>
            </div><!-- .entry-footer -->
            <?php
        }
    }
}

new WPrealizerPage();

