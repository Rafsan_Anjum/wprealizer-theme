<?php
if ( ! function_exists( 'wprealizer_post_thumbnail' ) ) {
    /**
     * Display post thumbnail
     *
     * @since 1.0.0
     */
    function wprealizer_post_thumbnail( $size = 'full' ) {
        if ( has_post_thumbnail() ) {
            the_post_thumbnail( $size );
        }
    }
}