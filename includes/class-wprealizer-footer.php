<?php
/**
 * WP Realizer Footer Class
 *
 * @since 1.0.0
 *
 * @package wprealizer
 */

if ( ! defined( 'ABSPATH' ) ) {
    return;
}

if ( ! class_exists( 'WPrealizerFooter' ) ) {
    class WPrealizerFooter {

        /**
         * WPrealizerFooter constructor
         *
         * @since 1.0.0
         */
        public function __construct() {
            add_action( 'wprealizer_before_footer', [ $this, 'before_footer' ] );
            add_action( 'wprealizer_footer', [ $this, 'footer' ] );
            add_action( 'wprealizer_after_footer', [ $this, 'after_footer' ] );
        }

        /**
         * Before footer area setups
         *
         * @since 1.0.0
         *
         * @return void
         */
        public function before_footer() {
            // use for future
        }

        /**
         * Main footer content setups
         *
         * @since 1.0.0
         *
         * @return void
         */
        public function footer() {
            $rows    = intval( apply_filters( 'wprealizer_footer_widget_rows', 1 ) );
            $regions = intval( apply_filters( 'wprealizer_footer_widget_columns', 4 ) );

            for ( $row = 1; $row <= $rows; $row++ ) :

                // Defines the number of active columns in this footer row.
                for ( $region = $regions; 0 < $region; $region-- ) {
                    if ( is_active_sidebar( 'footer-' . esc_attr( $region + $regions * ( $row - 1 ) ) ) ) {
                        $columns = $region;
                        break;
                    }
                }

                if ( isset( $columns ) ) :
                    ?>
                    <div class=<?php echo '"footer-widgets row-' . esc_attr( $row ) . ' col-' . esc_attr( $columns ) . ' fix"'; ?>>
                        <?php
                        for ( $column = 1; $column <= $columns; $column++ ) :
                            $footer_n = $column + $regions * ( $row - 1 );

                            if ( is_active_sidebar( 'footer-' . esc_attr( $footer_n ) ) ) :
                                ?>
                                <div class="block footer-widget-<?php echo esc_attr( $column ); ?>">
                                    <?php dynamic_sidebar( 'footer-' . esc_attr( $footer_n ) ); ?>
                                </div>
                            <?php
                            endif;
                        endfor;
                        ?>
                    </div><!-- .footer-widgets.row-<?php echo esc_attr( $row ); ?> -->
                    <?php
                    unset( $columns );
                endif;
            endfor;
        }

        /**
         * After footer area setups
         *
         * @since 1.0.0
         *
         * @return void
         */
        public function after_footer() {
            // use for future
        }
    }
}

new WPrealizerFooter();

