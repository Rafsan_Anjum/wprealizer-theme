<?php
/**
 * wprealizer Customizer Class
 *
 * @package  wprealizer
 * @since    2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'wprealizer_Customizer' ) ) :

	/**
	 * The wprealizer Customizer class
	 */
	class wprealizer_Customizer {

		/**
		 * Setup class.
		 *
		 * @since 1.0
		 */
		public function __construct() {
			add_action( 'customize_register', array( $this, 'wprealizer_customize_register' ), 10 );
			add_action( 'wp_enqueue_scripts', array( $this, 'add_customizer_css' ), 130 );
			add_action( 'customize_register', array( $this, 'edit_default_customizer_settings' ), 99 );
            add_action( 'init', array( $this, 'default_theme_mod_values' ), 10 );

		}

		/**
		 * Returns an array of the desired default wprealizer Options
		 *
		 * @return array
		 */
		public function get_wprealizer_default_setting_values() {
			return apply_filters(
				'wprealizer_setting_default_values',
				$args = array(
					'wprealizer_header_background_color' => '#ffffff',
					'wprealizer_header_text_color'       => '#404040',
					'wprealizer_header_link_color'       => '#333333',
				)
			);
		}

        /**
		 * Adds a value to each wprealizer setting if one isn't already present.
		 *
		 * @uses get_wprealizer_default_setting_values()
		 */
		public function default_theme_mod_values() {
			foreach ( $this->get_wprealizer_default_setting_values() as $mod => $val ) {
				add_filter( 'theme_mod_' . $mod, array( $this, 'get_theme_mod_value' ), 10 );
			}
		}

		/**
		 * Get theme mod value.
		 *
		 * @param string $value Theme modification value.
		 * @return string
		 */
		public function get_theme_mod_value( $value ) {
			$key = substr( current_filter(), 10 );

			$set_theme_mods = get_theme_mods();

			if ( isset( $set_theme_mods[ $key ] ) ) {
				return $value;
			}

			$values = $this->get_wprealizer_default_setting_values();

			return isset( $values[ $key ] ) ? $values[ $key ] : $value;
		}

		/**
		 * Set Customizer setting defaults.
		 * These defaults need to be applied separately as child themes can filter wprealizer_setting_default_values
		 *
		 * @param  array $wp_customize the Customizer object.
		 * @uses   get_wprealizer_default_setting_values()
		 */
		public function edit_default_customizer_settings( $wp_customize ) {
			foreach ( $this->get_wprealizer_default_setting_values() as $mod => $val ) {
				$wp_customize->get_setting( $mod )->default = $val;
			}
		}
		
		/**
		 * Returns an array of the desired default wprealizer Options
		 *
		 * @return array
		 */

            function wprealizer_customize_register( $wp_customize ) {
            
            /*******************************************
            Color scheme
            ********************************************/
            
            // add the section to contain the settings
            $wp_customize->add_section( 'textcolors' , array(
                'title' =>  'Color Scheme',
            ) );
          
            // main color ( site title, h1, h2, h4. h6, widget headings, nav links, footer headings )
            $txtcolors[] = array(
                'slug'=>'color_scheme_1', 
                'default' => '#000',
                'label' => 'Main Color'
            );
            
            // secondary color ( site description, sidebar headings, h3, h5, nav links on hover )
            $txtcolors[] = array(
                'slug'=>'color_scheme_2', 
                'default' => '#666',
                'label' => 'Secondary Color'
            );
            
            // link color
            $txtcolors[] = array(
                'slug'=>'link_color', 
                'default' => '#008AB7',
                'label' => 'Link Color'
            );
            
            // link color ( hover, active )
            $txtcolors[] = array(
                'slug'=>'hover_link_color', 
                'default' => '#9e4059',
                'label' => 'Link Color (on hover)'
            );


             // add the settings and controls for each color
            foreach( $txtcolors as $txtcolor ) {
            
                // SETTINGS
                $wp_customize->add_setting(
                    $txtcolor['slug'], array(
                        'default' => $txtcolor['default'],
                        'type' => 'option', 
                        'capability' =>  'edit_theme_options'
                    )
                );

                // CONTROLS
                $wp_customize->add_control(
                    new WP_Customize_Color_Control(
                        $wp_customize,
                        $txtcolor['slug'], 
                        array('label' => $txtcolor['label'], 
                        'section' => 'textcolors',
                        'settings' => $txtcolor['slug'])
                    )
                );
     
}
               /**
			 * Header Background
			 */
			$wp_customize->add_setting(
				'wprealizer_header_background_color',
				array(
					'default'           => apply_filters( 'wprealizer_default_header_background_color', '#2c2d33' ),
					'sanitize_callback' => 'sanitize_hex_color',
				)
			);

			$wp_customize->add_control(
				new WP_Customize_Color_Control(
					$wp_customize,
					'wprealizer_header_background_color',
					array(
						'label'    => __( 'Background color', 'wprealizer' ),
						'section'  => 'header_image',
						'settings' => 'wprealizer_header_background_color',
						'priority' => 15,
					)
				)
			);

			/**
			 * Header text color
			 */
			$wp_customize->add_setting(
				'wprealizer_header_text_color',
				array(
					'default'           => apply_filters( 'wprealizer_default_header_text_color', '#9aa0a7' ),
					'sanitize_callback' => 'sanitize_hex_color',
				)
			);

			$wp_customize->add_control(
				new WP_Customize_Color_Control(
					$wp_customize,
					'wprealizer_header_text_color',
					array(
						'label'    => __( 'Text color', 'wprealizer' ),
						'section'  => 'header_image',
						'settings' => 'wprealizer_header_text_color',
						'priority' => 20,
					)
				)
			);

			/**
			 * Header link color
			 */
			$wp_customize->add_setting(
				'wprealizer_header_link_color',
				array(
					'default'           => apply_filters( 'wprealizer_default_header_link_color', '#d5d9db' ),
					'sanitize_callback' => 'sanitize_hex_color',
				)
			);

			$wp_customize->add_control(
				new WP_Customize_Color_Control(
					$wp_customize,
					'wprealizer_header_link_color',
					array(
						'label'    => __( 'Link color', 'wprealizer' ),
						'section'  => 'header_image',
						'settings' => 'wprealizer_header_link_color',
						'priority' => 30,
					)
				)
			);

            }

		/**
		 * Add CSS in <head> for styles handled by the theme customizer
		 *
		 * @since 1.0.0
		 * @return void
		 */
		public function add_customizer_css() {
			wp_add_inline_style( 'wprealizer-style', $this->get_css() );
		}

            /**
		 * Get all of the wprealizer theme mods.
		 *
		 * @return array $wprealizer_theme_mods The wprealizer Theme Mods.
		 */
		public function get_wprealizer_theme_mods() {
			$wprealizer_theme_mods = array(
				'header_background_color'     => get_theme_mod( 'wprealizer_header_background_color' ),
				'header_link_color'           => get_theme_mod( 'wprealizer_header_link_color' ),
				'header_text_color'           => get_theme_mod( 'wprealizer_header_text_color' ),
			);

			return apply_filters( 'wprealizer_theme_mods', $wprealizer_theme_mods );
		}

        /**
		 * Get Customizer css.
		 *
		 * @see get_wprealizer_theme_mods()
		 * @return array $styles the css
		 */
		public function get_css() {
			$wprealizer_theme_mods = $this->get_wprealizer_theme_mods();
			$brighten_factor       = apply_filters( 'wprealizer_brighten_factor', 25 );
			$darken_factor         = apply_filters( 'wprealizer_darken_factor', -25 );

			$styles = '
			.main-navigation ul li a, .main-navigation ul li a:hover,
			.site-title a,
			ul.menu li a,
			.site-branding h1 a,
			button.menu-toggle,
			button.menu-toggle:hover,
			.handheld-navigation .dropdown-toggle {
				color: ' . $wprealizer_theme_mods['header_link_color'] . ';
			}

			button.menu-toggle,
			button.menu-toggle:hover {
				border-color: ' . $wprealizer_theme_mods['header_link_color'] . ';
			}

			.site-header,
			.secondary-navigation ul ul,
			.main-navigation ul.menu > li.menu-item-has-children:after,
			.secondary-navigation ul.menu ul,
			.wprealizer-handheld-footer-bar,
			.wprealizer-handheld-footer-bar ul li > a,
			.wprealizer-handheld-footer-bar ul li.search .site-search,
			button.menu-toggle,
			button.menu-toggle:hover {
				background-color: ' . $wprealizer_theme_mods['header_background_color'] . ';
			}

			p.site-description,
			.site-header,
			.wprealizer-handheld-footer-bar {
				color: ' . $wprealizer_theme_mods['header_text_color'] . ';
			}

			button.menu-toggle:after,
			button.menu-toggle:before,
			button.menu-toggle span:before {
				background-color: ' . $wprealizer_theme_mods['header_link_color'] . ';
			}


			.site-footer .wprealizer-handheld-footer-bar a:not(.button):not(.components-button) {
				color: ' . $wprealizer_theme_mods['header_link_color'] . ';
			}

			.secondary-navigation ul.menu a {
			    color: ' . $wprealizer_theme_mods['header_text_color'] . ';
			}
			}';

			return apply_filters( 'wprealizer_customizer_css', $styles );
		}
	}

endif;

return new wprealizer_Customizer();
