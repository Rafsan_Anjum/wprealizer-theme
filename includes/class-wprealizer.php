<?php
/**
 * WP Realizer Main Class
 *
 * @since 1.0.0
 *
 * @package wprealizer
 */

if ( ! defined( 'ABSPATH' ) ) {
    return;
}

if ( ! class_exists( 'WPrealizer' ) ) {
    class WPrealizer {

        /**
         * WPrealizer constructor
         *
         * @since 1.0.0
         */
        public function __construct() {
            add_action( 'after_setup_theme', [ $this, 'setups' ] );
            add_action( 'wp_enqueue_scripts', [ $this, 'frontend_enqueue_scripts' ] );
            add_action( 'admin_enqueue_scripts', [ $this, 'admin_enqueue_scripts' ] );
            add_action( 'widgets_init', [ $this, 'widgets_init' ] );
            add_filter( 'body_class', [ $this, 'body_classes' ] );
        }

        /**
         * Theme Setups
         *
         * @since 1.0.0
         *
         * @return void
         */
        public function setups() {
            // Loads wp-content/languages/themes/wprealizer-it_IT.mo
            load_theme_textdomain( 'wprealizer', trailingslashit( WP_LANG_DIR ) . 'themes' );

            // Loads wp-content/themes/wprealizer/languages/it_IT.mo
            load_theme_textdomain( 'wprealizer', get_template_directory() . '/languages' );

            // Add default posts and comments RSS feed links to head
            add_theme_support( 'automatic-feed-links' );

            // Enable support for Post Thumbnails on posts and pages
            add_theme_support( 'post-thumbnails' );

            // Theme custom header support
            add_theme_support(
                'custom-header',
                apply_filters(
                    'wprealizer_custom_header_args',
                    array(
                        'default-image' => '',
                        'header-text'   => false,
                        'width'         => 1950,
                        'height'        => 500,
                        'flex-width'    => true,
                        'flex-height'   => true,
                    )
                )
            );

            // Enable support for site logo
            add_theme_support(
                'custom-logo',
                apply_filters(
                    'wprealizer_custom_logo_args',
                    array(
                        'height'      => 150,
                        'width'       => 200,
                        'flex-width'  => true,
                        'flex-height' => true,
                    )
                )
            );

            // Theme custom background support
            add_theme_support(
                'custom-background',
                apply_filters(
                    'wprealizer_custom_background_args',
                    array(
                        'default-color' => apply_filters( 'wprealizer_default_background_color', 'ffffff' ),
                        'default-image' => '',
                    )
                )
            );

            // Theme HTML5 support
            add_theme_support(
                'html5',
                apply_filters(
                    'wprealizer_html5_supports_args',
                    array(
                        'search-form',
                        'comment-form',
                        'comment-list',
                        'widgets',
                        'gallery',
                        'caption',
                        'style',
                        'script',
                    )
                )
            );

			register_nav_menus(
				apply_filters(
					'storefront_register_nav_menus',
					array(
						'primary'   => __( 'Primary Menu', 'storefront' ),
						'secondary' => __( 'Secondary Menu', 'storefront' ),
						'handheld'  => __( 'Handheld Menu', 'storefront' ),
					)
				)
			);
            // Theme custom font sizes
            add_theme_support(
                'editor-font-sizes',
                apply_filters(
                    'wprealizer_register_editor_font_size_args',
                    array(
                        array(
                            'name' => __( 'Small', 'wprealizer' ),
                            'size' => 14,
                            'slug' => 'small',
                        ),
                        array(
                            'name' => __( 'Normal', 'wprealizer' ),
                            'size' => 16,
                            'slug' => 'normal',
                        ),
                        array(
                            'name' => __( 'Medium', 'wprealizer' ),
                            'size' => 23,
                            'slug' => 'medium',
                        ),
                        array(
                            'name' => __( 'Large', 'wprealizer' ),
                            'size' => 26,
                            'slug' => 'large',
                        ),
                        array(
                            'name' => __( 'Huge', 'wprealizer' ),
                            'size' => 37,
                            'slug' => 'huge',
                        ),
                    )
                )
            );

            // Others theme supports
            add_theme_support( 'title-tag' );
            add_theme_support( 'customize-selective-refresh-widgets' );
            add_theme_support( 'wp-block-styles' );
            add_theme_support( 'align-wide' );
            add_theme_support( 'editor-styles' );
        }

        /**
         * Theme frontend enqueue scripts loading
         *
         * @since 1.0.0
         *
         * @return void
         */
        public function frontend_enqueue_scripts() {
            // Styles loading
            wp_enqueue_style( 'wprealizer-style', get_template_directory_uri() . '/style.css', '', time() );
            wp_enqueue_style( 'wprealizer-theme-style', get_template_directory_uri() . '/assets/css/style.css', '', time() );

            // Scripts loading
            wp_enqueue_script( 'wprealizer-scripts', get_template_directory_uri() . '/assets/js/scripts.js', array(), time(), true );
        
            // Navigation Script loading
            	
            wp_enqueue_script( 'wpb_togglemenu', get_template_directory_uri() . '/assets/js/navigation.js', array('jquery'), '20160909', true );
            // wp_enqueue_script( 'wprealizer-navigation-scripts', get_template_directory_uri() . '/assets/js/navigation.js', array(), time(), true );
        }


        /**
         * Theme admin enqueue scripts loading
         *
         * @since 1.0.0
         *
         * @return void
         */
        public function admin_enqueue_scripts() {
            // Styles loading
            wp_enqueue_style( 'wprealizer-admin-style', get_template_directory_uri() . '/assets/css/admin-style.css', '', time() );

            // Scripts loading
            wp_enqueue_script( 'wprealizer-scripts', get_template_directory_uri() . '/assets/js/admin-scripts.js', array(), time(), true );
        }
        /**
         * Register widget area
         *
         * @since 1.0.0
         *
         * @return void
         */
        public function widgets_init() {
            $sidebar_args['sidebar'] = array(
                'name'        => __( 'Sidebar', 'wprealizer' ),
                'id'          => 'sidebar-1',
                'description' => '',
            );

            $sidebar_args['header'] = array(
                'name'        => __( 'Below Header', 'wprealizer' ),
                'id'          => 'header-1',
                'description' => __( 'Widgets added to this region will appear beneath the header and above the main content', 'wprealizer' ),
            );

            $rows    = intval( apply_filters( 'wprealizer_footer_widget_rows', 1 ) );
            $regions = intval( apply_filters( 'wprealizer_footer_widget_columns', 4 ) );

            for ( $row = 1; $row <= $rows; $row++ ) {
                for ( $region = 1; $region <= $regions; $region++ ) {
                    $footer_n = $region + $regions * ( $row - 1 ); // Defines footer sidebar ID.
                    $footer   = sprintf( 'footer_%d', $footer_n );

                    if ( 1 === $rows ) {
                        /* translators: 1: column number */
                        $footer_region_name = sprintf( __( 'Footer Column %1$d', 'wprealizer' ), $region );

                        /* translators: 1: column number */
                        $footer_region_description = sprintf( __( 'Widgets added here will appear in column %1$d of the footer.', 'wprealizer' ), $region );
                    } else {
                        /* translators: 1: row number, 2: column number */
                        $footer_region_name = sprintf( __( 'Footer Row %1$d - Column %2$d', 'wprealizer' ), $row, $region );

                        /* translators: 1: column number, 2: row number */
                        $footer_region_description = sprintf( __( 'Widgets added here will appear in column %1$d of footer row %2$d.', 'wprealizer' ), $region, $row );
                    }

                    $sidebar_args[ $footer ] = array(
                        'name'        => $footer_region_name,
                        'id'          => sprintf( 'footer-%d', $footer_n ),
                        'description' => $footer_region_description,
                    );
                }
            }

            $sidebar_args = apply_filters( 'wprealizer_sidebar_args', $sidebar_args );

            foreach ( $sidebar_args as $sidebar => $args ) {
                $widget_tags = array(
                    'before_widget' => '<div id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</div>',
                    'before_title'  => '<span class="gamma widget-title">',
                    'after_title'   => '</span>',
                );

                $filter_hook = sprintf( 'wprealizer_%s_widget_tags', $sidebar );
                $widget_tags = apply_filters( $filter_hook, $widget_tags );

                if ( is_array( $widget_tags ) ) {
                    register_sidebar( $args + $widget_tags );
                }
            }
        }

        /**
         * Body classes modify
         *
         * @since 1.0.0
         *
         * @param array $classes
         *
         * @return array $classes
         */
        public function body_classes( $classes ) {
            $classes[] = 'wprealizer-body-template';

            return $classes;
        }
    }
}

new WPrealizer();
