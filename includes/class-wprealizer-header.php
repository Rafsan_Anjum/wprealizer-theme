<?php
/**
 * WP Realizer Header Class
 *
 * @since 1.0.0
 *
 * @package wprealizer
 */

if ( ! defined( 'ABSPATH' ) ) {
    return;
}

if ( ! class_exists( 'WPrealizerHeader' ) ) {
    class WPrealizerHeader {

        /**
         * WPrealizerPage constructor
         *
         * @since 1.0.0
         */
        public function __construct() {
            add_action( 'wprealizer_head', [ $this, 'head' ] );
            add_action( 'wprealizer_after_body_start', [ $this, 'after_body_start' ] );
            add_action( 'wprealizer_before_header', [ $this, 'before_header' ] );
            add_action( 'wprealizer_header', [ $this, 'header' ] );
            add_action( 'wprealizer_before_content', [ $this, 'before_content' ] );
            add_action( 'wprealizer_start_content', [ $this, 'start_content' ] );
        }

        /**
         * Head area setups
         *
         * @since 1.0.0
         *
         * @return void
         */
        public function head() {
            // use for head
        }

        /**
         * After body area setups
         *
         * @since 1.0.0
         *
         * @return void
         */
        public function after_body_start() {
            // use for after_body_start
        }

        /**
         * Before header area setups
         *
         * @since 1.0.0
         *
         * @return void
         */
        public function before_header() {
            // use for before_header
        }

        /**
         * Main header content setups
         *
         * @since 1.0.0
         *
         * @return void
         */
        public function header() {
            echo '<div class="col-full">';
                $this->header_logo_ara();
            echo '</div>';
            $this->header_primary_navbar();
        }

        /**
         * Main header content setups
         *
         * @since 1.0.0
         *
         * @return void
         */
        public function header_logo_ara() {
            ?>
            <div class="site-branding">
                <?php
                if ( function_exists( 'the_custom_logo' ) && has_custom_logo() ) {
                    $logo = get_custom_logo();
                    $html = is_home() ? '<h1 class="logo">' . $logo . '</h1>' : $logo;
                } else {
                    $tag = is_home() ? 'h1' : 'div';

                    $html = '<' . esc_attr( $tag ) . ' class="beta site-title"><a href="' . esc_url( home_url( '/' ) ) . '" rel="home">' . esc_html( get_bloginfo( 'name' ) ) . '</a></' . esc_attr( $tag ) . '>';

                    if ( '' !== get_bloginfo( 'description' ) ) {
                        $html .= '<p class="site-description">' . esc_html( get_bloginfo( 'description', 'display' ) ) . '</p>';
                    }
                }
                echo $html;
                ?>
            </div>
            <?php
        }

        /**
         * Before main content area setups
         *
         * @since 1.0.0
         *
         * @return void
         */
        public function header_primary_navbar() {
            ?>
            <div class="wprealizer-primary-navigation">
                <div class="col-full">
                  <nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Primary Navigation', 'wprealizer' ); ?>">
                        <button class="menu-toggle" aria-controls="site-navigation" aria-expanded="false"><span><?php echo esc_html( apply_filters( 'wprealizer_menu_toggle_text', __( 'Menu', 'wprealizer' ) ) ); ?></span></button>
                        <?php
                        wp_nav_menu(
                            array(
                                'theme_location'  => 'primary',
                                'container_class' => 'primary-navigation',
                            )
                        );
                        wp_nav_menu(
                            array(
                                'theme_location'  => 'handheld',
                                'container_class' => 'handheld-navigation',
                            )
                        );
                         ?>
                    </nav>
            
                </div>
            </div>
            <?php
        }

        /**
         * Before main content area setups
         *
         * @since 1.0.0
         *
         * @return void
         */
        public function before_content() {
            // use for before_content
        }

        /**
         * Content start area setups
         *
         * @since 1.0.0
         *
         * @return void
         */
        public function start_content() {
            // use for start_content
        }
    }
}

new WPrealizerHeader();

