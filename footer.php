<?php
/**
 * The is for main the footer
 *
 * @package wprealizer
 */
?>

</div><!-- .col-full -->
</div><!-- #content -->

<?php
/**
 * WP Realizer Before Main Footer
 *
 * @since 1.0.0
 */
do_action( 'wprealizer_before_footer' );
?>

<footer id="colophon" class="site-footer">
    <div class="col-full">

        <?php
        /**
         * WP Realizer Before Main Footer
         *
         * @since 1.0.0
         */
        do_action( 'wprealizer_footer' );
        ?>
    </div><!-- .col-full -->
</footer><!-- #colophon -->

<?php
/**
 * WP Realizer Before Main Footer
 *
 * @since 1.0.0
 */
do_action( 'wprealizer_after_footer' );
?>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
