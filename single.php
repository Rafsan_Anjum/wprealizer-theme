<?php
/**
 * The template for single page
 *
 * @package wprealizer
 */

get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

            <?php
            while ( have_posts() ) :
                the_post();

                /**
                 * WP Realizer Before Page Hooks
                 *
                 * @since 1.0.0
                 */
                do_action( 'wprealizer_page_before' );

                get_template_part( 'template-parts/content', 'single' );

                /**
                 * WP Realizer After Page Hooks
                 *
                 * @since 1.0.0
                 */
                do_action( 'wprealizer_page_after' );

            endwhile; // End of the loop.
            ?>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php
do_action( 'wprealizer_main_sidebar' );
get_footer();
