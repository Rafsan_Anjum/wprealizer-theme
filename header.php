<?php
/**
 * This is for header
 *
 * @package wprealizer
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <?php wp_head(); ?>

    <?php
    /**
     * WP Realizer End of Head Area Hooks
     *
     * @since 1.0.0
     */
    do_action( 'wprealizer_head' );
    ?>
</head>

<body <?php body_class(); ?>>

<?php wp_body_open(); ?>

<?php do_action( 'wprealizer_after_body_start' ); ?>

<div id="page" class="site">

    <?php
    /**
     * WP Realizer Before Header Area Hooks
     *
     * @since 1.0.0
     */
    do_action( 'wprealizer_before_header' );
    ?>

    <header id="main-to-main-head" class="site-header">
        <?php
        /**
         * WP Realizer Header Area Hooks
         *
         * @since 1.0.0
         */
        do_action( 'wprealizer_header' );
        ?>
    </header>

    <?php
    /**
     * WP Realizer Before Content Area Hooks
     *
     * @since 1.0.0
     */
    do_action( 'wprealizer_before_content' );
    ?>
    <div id="content" class="site-content">
        <div class="col-full">

        <?php
        /**
         * WP Realizer Before Content Area Hooks
         *
         * @since 1.0.0
         */
        do_action( 'wprealizer_start_content' );
        ?>