<?php
/**
 * The template for index page
 *
 * @package wprealizer
 */

get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

            <?php
            while ( have_posts() ) :
                the_post();

                /**
                 * WP Realizer Before Page Hooks
                 *
                 * @since 1.0.0
                 */
                do_action( 'wprealizer_page_before' );

                get_template_part( 'template-parts/content', 'home' );

                /**
                 * WP Realizer After Page Hooks
                 *
                 * @since 1.0.0
                 */
                do_action( 'wprealizer_page_after' );

            endwhile; // End of the loop.
            ?>
            <?php if (get_theme_mod('basic-author-callout-display') == 'Yes') { ?>
<div class="row row-padding author">
    <div class="col-6 author-image">
    <img src="<?php echo wp_get_attachment_url(get_theme_mod('basic-author-callout-image')) ?>" alt="Author Image">
    </div>
    <div class="col-6 author-content">
        <?php 
            $authorText = get_theme_mod('basic-author-callout-text');
            if ($authorText != '') {
                echo wpautop($authorText);
            } else {
                echo "Edit this by going to your Dashboard -> Appearance -> Customise -> Author Editor";
            }
        ?>
    </div>
</div> 
<?php } ?>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php
do_action( 'wprealizer_main_sidebar' );
get_footer();
