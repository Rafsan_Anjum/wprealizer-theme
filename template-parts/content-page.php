<?php
/**
 * Template used to display post content on pages
 *
 * @package wprealizer
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php
    /**
     * WP Realizer page top hooks
     *
     * @version 1.0.0
     * @package wprealizer
     */
    do_action( 'wprealizer_page_top' );

    /**
     * WP Realizer page main content hooks
     *
     * @version 1.0.0
     * @package wprealizer
     */
    do_action( 'wprealizer_page_content' );

    /**
     * WP Realizer page bottom hooks
     *
     * @version 1.0.0
     * @package wprealizer
     */
    do_action( 'wprealizer_page_bottom' );
    ?>

</article><!-- #post-## -->
