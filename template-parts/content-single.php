<?php
/**
 * Template used to display post content on single pages
 *
 * @package wprealizer
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php
    /**
     * WP Realizer single post top hooks
     *
     * @version 1.0.0
     * @package wprealizer
     */
    do_action( 'wprealizer_single_post_top' );

    /**
     * WP Realizer single post main content hooks
     *
     * @version 1.0.0
     * @package wprealizer
     */
    do_action( 'wprealizer_single_post_content' );

    /**
     * WP Realizer single post bottom hooks
     *
     * @version 1.0.0
     * @package wprealizer
     */
    do_action( 'wprealizer_single_post_bottom' );
    ?>

</article><!-- #post-## -->
