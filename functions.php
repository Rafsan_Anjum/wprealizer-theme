<?php
/**
 * WP Realizer Main Function
 *
 * @package wprealizer
 */

define( 'WPREALIZER_THEME_VERSION', '1.0.0' );

/**
 * Load main theme class where have multiple
 * setups when plugin activate
 *
 * @since 1.0.0
 * @package wprealizer
 */
require 'includes/class-wprealizer.php';

/**
 * Load customizer  class 
 *
 * @since 1.0.0
 * @package wprealizer
 */
require 'includes/class-wprealizer-customizer.php';

/**
 * Load main theme custom functions
 *
 * @since 1.0.0
 * @package wprealizer
 */
require 'includes/wprealizer-functions.php';

/**
 * Load main theme custom widgets
 *
 * @since 1.0.0
 * @package wprealizer
 */
require 'includes/CustomWidget.php';

/**
 * Loading main theme templates contents
 *
 * @since 1.0.0
 * @package wprealizer
 */
require 'includes/class-wprealizer-header.php';
require 'includes/class-wprealizer-footer.php';
require 'includes/class-wprealizer-page.php';
require 'includes/class-wprealizer-single.php';
require 'includes/class-wprealizer-home.php';